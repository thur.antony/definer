#! /usr/bin/python3
#definer
import os
import requests
import bs4
import datetime
from termcolor import colored
from tinydb import TinyDB, Query

def define(word, db):
    page_base = "https://en.oxforddictionaries.com/definition/"
    page_source = requests.get(page_base + word)
    if page_source.status_code == 200:
        soup = bs4.BeautifulSoup(page_source.content,'html.parser')
        soup.prettify()
        definition = soup.find('span', {'class' : 'ind'})
        
        try:
            definition = definition.get_text()
            
            print(colored(definition, 'magenta'))
            
            q = Query()
            if not db.search(q.word == word):
                now = datetime.datetime.today().strftime('%y%m%d')
                db.insert({'date' : now, 'word' : word, 'def' : definition})
            
        except:
            print(word + colored(' - is wrong word\n', 'red'))

def main():
    os.system('clear')
    #usage
    print(colored('Definer', 'yellow'))
    print("[word] OR [q == quit] OR [s == show] OR [h == help]")
    
    db = TinyDB('dict.json')
    
    #main loop
    while 1:
        input_str = input(colored('?:', 'yellow'))
        if input_str == 'h':
            print("[word] OR [q == quit] OR [s == show] OR [h == help]")
        elif input_str == 'q':
            os.system('clear')
            break
        elif input_str == 's':
            os.system('clear')
            #input_str = input(f"which date? {colored('yymmdd', 'yellow')}:") #python3.7
            input_str = input("which date? {}:".format(colored('yymmdd', 'yellow')))
            
            q = Query()
            wordlist = db.search(q.date == input_str)
            if wordlist:
                os.system('clear')
                print(colored(input_str, 'yellow'))
                for item in wordlist:
                    #print(f"{colored(item['word'], 'yellow')} - {colored(item['def'], 'magenta')}") #python3.7
                    print("{} - {}".format(colored(item['word'], 'yellow'), colored(item['def'], 'magenta')))
            else:
                print(colored('empty day', 'yellow'))
        else:
            define(input_str, db)

if __name__ == '__main__':
    main()